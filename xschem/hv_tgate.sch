v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 350 -430 350 -220 { lab=A}
N 410 -430 410 -220 { lab=B}
N 380 -430 380 -370 { lab=VHVPWR}
N 380 -270 380 -220 { lab=VGND}
N 380 -270 450 -270 { lab=VGND}
N 380 -510 380 -470 { lab=not_pgate}
N 290 -180 380 -180 { lab=pgate}
N -190 -420 -190 -400 { lab=VGND}
N -190 -400 -30 -400 { lab=VGND}
N -30 -420 -30 -400 { lab=VGND}
N -190 -600 -190 -480 { lab=not_pgate}
N -30 -600 -30 -480 { lab=pgate}
N -150 -630 -120 -630 { lab=pgate}
N -120 -630 -120 -580 { lab=pgate}
N -120 -580 -30 -580 { lab=pgate}
N -110 -630 -70 -630 { lab=not_pgate}
N -190 -570 -110 -570 { lab=not_pgate}
N -110 -630 -110 -570 { lab=not_pgate}
N -190 -690 -190 -660 { lab=VHVPWR}
N -190 -690 -30 -690 { lab=VHVPWR}
N -30 -690 -30 -660 { lab=VHVPWR}
N -200 -630 -190 -630 { lab=VHVPWR}
N -200 -660 -200 -630 { lab=VHVPWR}
N -200 -660 -190 -660 { lab=VHVPWR}
N -30 -630 -20 -630 { lab=VHVPWR}
N -20 -660 -20 -630 { lab=VHVPWR}
N -30 -660 -20 -660 { lab=VHVPWR}
N -190 -450 -170 -450 { lab=VGND}
N -170 -450 -170 -400 { lab=VGND}
N -30 -450 -10 -450 { lab=VGND}
N -10 -450 -10 -400 { lab=VGND}
N -30 -400 -10 -400 { lab=VGND}
N -390 -290 -70 -290 { lab=S_NOT}
N -70 -450 -70 -290 { lab=S_NOT}
N -470 -450 -230 -450 { lab=S}
N -190 -510 380 -510 { lab=not_pgate}
N 290 -490 290 -180 { lab=pgate}
N -30 -490 290 -490 { lab=pgate}
N -470 -290 -390 -290 { lab=S_NOT}
N -60 -760 -60 -690 { lab=VHVPWR}
N 270 -320 350 -320 { lab=A}
N 410 -320 480 -320 { lab=B}
N -40 -400 -40 -340 { lab=VGND}
N 380 -370 450 -370 { lab=VHVPWR}
N -40 -340 -30 -340 {
lab=VGND}
C {devices/lab_wire.sym} 450 -370 2 0 {name=l2 lab=VHVPWR}
C {devices/lab_wire.sym} 230 -510 0 0 {name=l13 lab=not_pgate}
C {devices/lab_wire.sym} 160 -490 0 0 {name=l3 lab=pgate}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 380 -200 3 0 {name=M1
L=0.5
W=10
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 380 -450 1 0 {name=M2
L=0.5
W=10
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} -50 -630 0 0 {name=M9
L=0.5
W=10
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} -170 -630 2 0 {name=M4
L=0.5
W=10
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} -210 -450 0 0 {name=x3
L=0.5
W=10
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} -50 -450 0 0 {name=x6
L=0.5
W=10
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {devices/ipin.sym} -470 -450 0 0 {name=p1 lab=S}
C {devices/ipin.sym} -470 -290 0 0 {name=p2 lab=S_NOT}
C {devices/iopin.sym} -60 -760 0 0 {name=p3 lab=VHVPWR}
C {devices/iopin.sym} 270 -320 2 0 {name=p4 lab=A}
C {devices/iopin.sym} 480 -320 0 0 {name=p5 lab=B}
C {devices/iopin.sym} -40 -340 0 0 {name=p6 lab=VGND}
C {devices/lab_wire.sym} 450 -270 2 0 {name=l1 lab=VGND}
