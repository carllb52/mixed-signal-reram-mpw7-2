v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 82.5 -230 132.5 -230 { lab=VSS}
N -520 -540 -520 -180 { lab=sl1}
N -280 -540 -280 -180 { lab=sl2}
N -590 -140 -190 -140 { lab=wl2}
N -590 -380 -190 -380 { lab=wl1}
N -430 -440 -430 -420 { lab=VSS}
N -190 -440 -190 -420 { lab=VSS}
N -430 -200 -430 -180 { lab=VSS}
N -190 -200 -190 -180 { lab=VSS}
N 132.5 -230 142.5 -230 { lab=VSS}
N -400 -100 -60 -100 { lab=bl2}
N -400 -340 -60 -340 { lab=bl1}
N -400 -420 -400 -340 { lab=bl1}
N -160 -420 -160 -340 { lab=bl1}
N -400 -180 -400 -100 { lab=bl2}
N -160 -180 -160 -100 { lab=bl2}
C {reram.sym} -250 -420 3 0 {name=XRERAM1
device=RERAM
m=1}
C {reram.sym} -490 -420 3 0 {name=XRERAM2
device=RERAM
m=1}
C {reram.sym} -250 -180 3 0 {name=XRERAM3
device=RERAM
m=1}
C {reram.sym} -490 -180 3 0 {name=XRERAM4
device=RERAM
m=1}
C {sky130_fd_pr/nfet_03v3_nvt.sym} -430 -400 1 1 {name=M1
L=0.5
W=1
nf=1 
mult=1
sa=0 sb=0 sd=0
model=nfet_03v3_nvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_03v3_nvt.sym} -190 -400 1 1 {name=M2
L=0.5
W=1
nf=1 
mult=1
sa=0 sb=0 sd=0
model=nfet_03v3_nvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_03v3_nvt.sym} -430 -160 1 1 {name=M3
L=0.5
W=1
nf=1
mult=1
sa=0 sb=0 sd=0
model=nfet_03v3_nvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_03v3_nvt.sym} -190 -160 1 1 {name=M4
L=0.5
W=1
nf=1
mult=1
sa=0 sb=0 sd=0
model=nfet_03v3_nvt
spiceprefix=X
}
C {devices/iopin.sym} -280 -540 0 0 {name=p1 lab=sl2}
C {devices/iopin.sym} -520 -540 0 0 {name=p2 lab=sl1}
C {devices/iopin.sym} -580 -380 2 0 {name=p3 lab=wl1}
C {devices/iopin.sym} -580 -140 2 0 {name=p4 lab=wl2}
C {devices/iopin.sym} -60 -340 0 0 {name=p5 lab=bl1}
C {devices/iopin.sym} -60 -100 0 0 {name=p6 lab=bl2}
C {devices/iopin.sym} 125 -230 0 0 {name=p9 lab=VSS}
C {devices/lab_wire.sym} 82.5 -230 0 0 {name=l1 lab=VSS}
C {devices/lab_wire.sym} -430 -200 1 0 {name=l2 lab=VSS}
C {devices/lab_wire.sym} -430 -440 1 0 {name=l3 lab=VSS}
C {devices/lab_wire.sym} -190 -440 1 0 {name=l4 lab=VSS}
C {devices/lab_wire.sym} -190 -200 1 0 {name=l5 lab=VSS}
C {devices/code_shown.sym} 55 -377.5 0 0 {name=seRAM only_toplevel=false value=".model RERAM sky130_fd_pr_reram__reram_cell"}
